<?php
/**
 * Plugin Name: Active plugin first
 * Version: 1.0.0
 * Plugin URI: https://gitlab.com/sdwp/plugins/wp-active-plugins-first
 * Description: This plugin add to plugins menu "Active plugins" item and set it default.
 * Author: Sergey Davydov
 * Author URI: https://gitlab.com/MrSwed
 * Tested up to: 5.4
 *
 * Text Domain: wp-active-plugins-first
 *
 * @package WordPress
 * @author Sergey Davydov
 * @since 1.0.0
 */

add_action( 'admin_menu', function () {
	add_submenu_page( "plugins.php", __( 'Active Plugins' ), __( 'Active Plugins' ), "activate_plugins", "plugins.php?plugin_status=active", '', 0 );
});
add_action( 'admin_print_footer_scripts', function(){
?><script>jQuery(document).ready(function($){
			jQuery('li:has(>a[href*="' + location.search + '"])', "#menu-plugins").addClass("current").siblings().removeClass("current");
		});</script><?php
});
